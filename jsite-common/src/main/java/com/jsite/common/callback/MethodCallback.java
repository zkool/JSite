package com.jsite.common.callback;

/**
 * 方法回调接口
 * @author liuruijun
 */
public interface MethodCallback {

	Object execute(Object... params);
	
}
